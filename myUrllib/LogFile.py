# coding = utf-8

from functools import wraps


def logfile(logfile):
    def logging_decorator(function):
        @wraps(function)
        def loggging(*args, **kwargs):
            log_string = function.__name__ + "haha"
            print(log_string)
            with open(logfile, 'a') as file:
                file.write(log_string + '\n')
            return function(*args, **kwargs)
        return loggging
    return logging_decorator


def logging_decorator(function):
    @wraps(function)
    def loggging(*args, **kwargs):
        log_string = function.__name__ + "haha"
        print(log_string)
        with open("out01.log", 'a') as file:
            file.write(log_string + '\n')
        return function(*args, **kwargs)
    return loggging


@logfile("out.log")
def test():
    print(" 7 ")


@logging_decorator
def test01():
    print("8")


if __name__ == '__main__':
    t = test()
    t = test01()
